import os
import configparser


class Config(object):
    def __init__(self):
        self.columns = {
            'day': 'B',
            'ucel': 'D',
            'prijem': 'E',
            'vydej': 'F',
            'rozpisstart': 'I',
            'rozpisend': 'R',
            'export_control': 'U',
            'name': 'V',
            'address': 'W',
            'kind': 'H',
            'offset': 5,
            'contsign': '.',
        }

        self.billing = {
            'fakturacni_udaje': 'neurcen',
            'celkem': 'Celkem:',
            'intstitute_text': 'Firma:',
            'total_words_text': 'Slovy: ',
            'vydal': 'Vydal:',
            'prijal': 'Přijal:',
            'prijmovy': 'Příjmový pokladní doklad č:',
            'vydajovy': 'Výdajový pokladní doklad č:',
            'datum_text': 'ze dne:',
            'prijatood': 'Přijato od: ',
            'vyplacenokomu': 'Vyplaceno komu: ',
            'ucel': "Účel platby: ",
            'kc': 'Kč',
            'korun': 'Korun českých',
        }

        config_path = os.path.join(os.path.dirname(__file__), 'config.txt')
        if os.path.exists(config_path):
            conf = configparser.ConfigParser()
            conf.read(config_path, encoding="utf8")

            for key in conf['COLUMNS'].keys():
                self.columns[key] = conf['COLUMNS'][key]

            if 'BILLING' in conf:
                for key in conf['BILLING'].keys():
                    if key == 'fakturacni_udaje':
                        txt = conf['BILLING'][key]
                        if '&nbsp;' in txt:
                            txt = txt.replace('&nbsp;', chr(160))
                        self.billing[key] = txt
                    else:
                        self.billing[key] = conf['BILLING'][key]

    @property
    def day(self):
        return self.columns['day']

    @property
    def ucel(self):
        return self.columns['ucel']

    @property
    def prijem(self):
        return self.columns['prijem']

    @property
    def vydej(self):
        return self.columns['vydej']

    @property
    def rozpis(self):
        return "{}{{}}:{}{{}}".format(
            self.columns['rozpisstart'],
            self.columns['rozpisend']
        )

    @property
    def export_control(self):
        return self.columns['export_control']

    @property
    def name(self):
        return self.columns['name']

    @property
    def address(self):
        return self.columns['address']

    @property
    def kind(self):
        return self.columns['kind']

    @property
    def bill(self):
        return self.billing

    @property
    def offset(self):
        return self.columns['offset']

    @property
    def contsign(self):
        return self.columns['contsign']


CONFIG = Config()
