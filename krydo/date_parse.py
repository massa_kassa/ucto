import re


def dateparse(line):
    m = re.search(
        '\d{1,2}[.,-\\\\/]\s*\d{1,2}[.,-\\\\/]\s*\d{2,4}',
        line
    )

    return m


if __name__ == '__main__':
    test_dates = [
        '1.1.2000',
        '1. 1. 2000',
        '1. 1. 00',
        '1 / 1 / 2000',
        '01 / 01 / 2000',
        '01.01.2000',
        '1 - 1 - 2000',
        '1\\1\\2000',
    ]
    for ii in test_dates:
        ma = dateparse(ii)
        if ma:
            print('OK {}'.format(ma.group()))
        else:
            print('failed for: {}'.format(ii))