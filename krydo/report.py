import os
from num2words import num2words
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib.pagesizes import landscape, portrait
from reportlab.lib.pagesizes import A5, A4
from reportlab.pdfbase.pdfmetrics import stringWidth, registerFont
from reportlab.pdfbase.ttfonts import TTFont
from krydo.config import CONFIG

registerFont(
    TTFont(
        'DejaVu',
        os.path.join(
            os.path.dirname(__file__),
            'data',
            'dejavu-fonts-ttf-2.37',
            'ttf',
            'DejaVuSerif.ttf'
        )
    )
)


class Bill(object):
    def __init__(self, target):
        self.upper = 5
        self.lower = 5
        self.left = 5
        self.right = 5

        self.lineadd = 1

        self.institute_box_height = 40
        self.institute_box_width = 70
        self.total_x_place = 100

        # self.ps = landscape(A5)
        self.ps = portrait(A4)
        self.fontsize = 12
        self.font = 'DejaVu'

        self.total_text = CONFIG.bill['celkem']
        self.total_words_text = CONFIG.bill['total_words_text']

        self.podpis_len = 40

        self.vydal = CONFIG.bill['vydal']
        self.vydal_p = 30
        self.v_len = stringWidth(self.vydal, self.font, self.fontsize)

        self.prijal = CONFIG.bill['prijal']
        self.prijal_p = self.vydal_p + self.podpis_len + 30 + self.v_len / mm
        self.p_len = stringWidth(self.prijal, self.font, self.fontsize)

        self.prijmovy = CONFIG.bill['prijmovy']
        self.prijmovy_len = stringWidth(self.prijmovy, self.font, self.fontsize)

        self.vydajovy = CONFIG.bill['vydajovy']
        self.vydajovy_len = stringWidth(self.vydajovy, self.font, self.fontsize)

        self.datum_text = CONFIG.bill['datum_text']

        self.prijatood = CONFIG.bill['prijatood']

        self.vyplacenokomu = CONFIG.bill['vyplacenokomu']

        self.ucel = CONFIG.bill['ucel']

        self.kc = CONFIG.bill['kc']

        self.korun = CONFIG.bill['korun']

        self.institute_txt = CONFIG.bill['fakturacni_udaje']

        self.c = None

        if isinstance(target, canvas.Canvas):
            self.c = target
        else:
            self.c = canvas.Canvas(
                target,
                pagesize=self.ps,
                initialFontName=self.font,
                initialFontSize=self.fontsize
            )

        # ==============================================================
        # draw basic report structure
        # ==============================================================
        self.c.rect(
            self.left * mm,
            self.lower * mm,
            self.ps[0] - (self.right + self.left) * mm,
            self.ps[1] - (self.lower + self.upper) * mm
        )

        # institute
        self.c.rect(
            self.left * mm,
            self.ps[1] - (self.upper + self.institute_box_height) * mm,
            self.institute_box_width * mm,
            self.institute_box_height * mm
        )

        # Firma: in text
        self.Firma_y = self.ps[1] - (self.upper + 1) * mm - self.fontsize
        self.c.drawString(
            (self.left + 3) * mm,
            self.Firma_y,
            'Firma:'
        )

        # celkem:
        c_len = self.stringwidth(self.total_text)
        self.line_xend = self.ps[0] - (self.right + 5) * mm
        self.c.drawString(
            self.total_x_place * mm - c_len - 3,
            self.ps[1] - (self.upper + self.institute_box_height) * mm + self.lineadd * mm,
            self.total_text
        )
        self.c.line(
            self.total_x_place * mm,
            self.ps[1] - (self.upper + self.institute_box_height) * mm,
            self.line_xend,
            self.ps[1] - (self.upper + self.institute_box_height) * mm,
        )

        # total in words
        s_len = self.stringwidth(self.total_words_text)
        self.vert_pos = self.ps[1] - (self.upper + self.institute_box_height + self.fontsize / 2) * mm
        self.c.drawString(
            (self.left + 3) * mm,
            self.vert_pos,
            self.total_words_text
        )
        self.total_line_xplace = (self.left + 3) * mm + s_len
        self.c.line(
            self.total_line_xplace,
            self.vert_pos - self.lineadd * mm,
            self.line_xend,
            self.vert_pos - self.lineadd * mm
        )

        # Vydal:
        self.c.drawString(
            self.vydal_p * mm,
            (self.lower + 3) * mm,
            self.vydal
        )
        self.c.line(
            self.vydal_p * mm + self.v_len,
            (self.lower + 2) * mm,
            (self.vydal_p + self.podpis_len) * mm + self.v_len,
            (self.lower + 2) * mm
        )
        # Prijal:
        self.c.drawString(
            self.prijal_p * mm,
            (self.lower + 3) * mm,
            self.prijal
        )
        self.c.line(
            self.prijal_p * mm + self.p_len,
            (self.lower + 2) * mm,
            (self.prijal_p + self.podpis_len) * mm + self.p_len,
            (self.lower + 2) * mm
        )

        # institute
        text_space = 3

        # ===================================================================
        # text wrapping code
        # ===================================================================

        # imaxw = (self.institute_box_width - text_space * 2) * mm
        # for i, txt in enumerate(self.wrap_text(self.institute_txt, imaxw)):
        #     self.c.drawString(
        #         (self.left + text_space) * mm,
        #         self.Firma_y - (self.fontsize + 2 * mm) * (i+1),
        #         txt
        #     )

        # ==================================================================
        # process the text directly as user enters it (allows char(10))
        # ==================================================================
        for i, txt in enumerate(self.institute_txt.split('\n')):
            self.c.drawString(
                (self.left + text_space) * mm,
                self.Firma_y - (self.fontsize + 2 * mm) * (i+1),
                txt
            )

        # ================================================================
        # init some other vars
        # ================================================================
        self.person_y = None

    def stringwidth(self, string):
        return stringWidth(string, self.font, self.fontsize)

    def draw_prijal(self, doklad_num, doklad_date, person_name, person_address, ucel_l, total):
        self.draw_upper(self.prijmovy, doklad_num, doklad_date)
        self.draw_person(self.prijatood, "{} {}".format(person_name, person_address))
        self.draw_ucel(ucel_l)
        self.draw_total(total)

    def draw_vydal(self, doklad_num, doklad_date, person_name, person_address, ucel_l, total):
        self.draw_upper(self.vydajovy, doklad_num, doklad_date)
        self.draw_person(self.vyplacenokomu, "{} {}".format(person_name, person_address))
        self.draw_ucel(ucel_l)
        self.draw_total(total)

    def draw_upper(self, start_string, doklad_num, doklad_date):
        self.c.drawString(
            (self.left + 5 + self.institute_box_width) * mm,
            self.ps[1] - (self.upper + self.fontsize/2) * mm,
            '{} {} {} {}'.format(start_string, doklad_num, self.datum_text, doklad_date)
        )

    def draw_person(self, payment_str, person_str):
        pv = self.stringwidth(payment_str)
        maxlen = self.line_xend - (self.left + 3) * mm - pv

        y = self.vert_pos - (self.fontsize + 2 * mm)
        self.c.drawString(
            (self.left + 3) * mm,
            y,
            payment_str
        )

        for i, txt in enumerate(self.wrap_text(person_str, maxlen)):
            y = self.vert_pos - (self.fontsize + 2 * mm) * (i+1)
            self.c.drawString(
                (self.left + 3) * mm + pv,
                y,
                txt
            )
            self.c.line(
                (self.left + 3) * mm + pv,
                y - 1 * mm,
                self.line_xend,
                y - 1 * mm,
            )
            self.person_y = y

    def draw_ucel(self, ucel_list):
        uw = self.stringwidth(self.ucel)
        self.c.drawString(
            (self.left + 3) * mm,
            self.person_y - (self.fontsize + 2 * mm),
            self.ucel
        )

        maxlen = self.line_xend - (self.left + 3) * mm - uw

        for i in range(len(ucel_list)):
            p = ucel_list[i]
            pw = self.stringwidth(p)
            if pw > maxlen:
                s3 = self.stringwidth('...')
                swl = [self.stringwidth(i + ' ') for i in p.split()]
                swl = [sum(swl[:i]) + s3 for i in range(len(swl))]
                s_max_i = max([i for i, j in enumerate(swl) if j < maxlen])
                p = ' '.join(p.split()[:s_max_i]) + '...'

            y = self.person_y - (self.fontsize + 2 * mm) * (i+1)
            self.c.drawString(
                (self.left + 3) * mm + uw,
                y,
                p
            )

    def draw_total(self, total):
        if isinstance(total, int):
            fmttext = ''
            tt = total
        elif isinstance(total, float):
            fmttext = '.2f'
            tt = round(total, 2)
        else:
            raise TypeError('total (zustatek) must be int or float')

        self.c.drawString(
            self.total_x_place * mm,
            self.ps[1] - (self.upper + self.institute_box_height) * mm + self.lineadd * mm,
            '{:{totfmt}} {}'.format(tt, self.kc, totfmt=fmttext)
        )

        # hack to overcome that num2words does not yet provide conversion of negative numbers
        try:
            total_text = num2words(tt, lang='cz')
        except ValueError:
            if tt < 0:
                total_text = 'mínus ' + num2words(abs(tt), lang='cz')
            else:
                raise

        self.c.drawString(
            self.total_line_xplace,
            self.vert_pos,
            '{} {}'.format(total_text, self.korun)
        )

    def wrap_text(self, text, maxw):
        # x = int(ceil(self.stringwidth(text) / maxw))
        p = text.split(' ')
        # for jj in range(x):
        while len(p) > 0:
            swl = [self.stringwidth(i + ' ') for i in p]
            swl = [sum(swl[:i + 1]) for i in range(len(swl))]
            s_max_i = max([i for i, j in enumerate(swl) if j < maxw]) + 1
            t = ' '.join(p[:s_max_i])
            yield t
            p = p[s_max_i:]

    def save(self):
        self.c.showPage()
        self.c.save()


class ReportError(Exception):
    pass


if __name__ == '__main__':
    with open('aa.pdf', 'wb') as fp:
        a = Bill(fp)
        a.draw_prijal(
            doklad_date='25.6.',
            doklad_num='p1',
            person_address='Za humny 31 Zemljamka 56021',
            person_name='Jarda z Cimic',
            ucel_l=['asd', 'nkjasd', 'oiad'],
            total=4021
        )
        a.c.showPage()
        a.c.save()
