from shutil import copy
import openpyxl
from krydo.process_ucto import extract_data
from krydo.config import CONFIG


def process_loaded_excel_new_workbook(loaded_excel, new_path, sheetname='TPK1'):
    # data in U, V and D
    # flags are in U
    rows = {
        'B': [],
        'C': [],
        'D': [],
        'E': [],
        'F': [],
        'H': [],
    }
    for i, r in enumerate(get_cover_row_or_block(loaded_excel[sheetname], 'U')):
        if i in range(CONFIG.offset - 1):
            continue
        if isinstance(r, list):
            # write data from cover
            colected_data = extract_data(loaded_excel[sheetname], r)

            # here was i by mistake - check if everything works as expected
            for j, (rozpis_key, rozpis_txt) in enumerate(colected_data.to_rows()):
                rows['B'].append(
                    colected_data.day
                )
                if j is 0:
                    rows['C'].append(
                        colected_data.doklad
                    )
                else:
                    rows['C'].append(
                        None
                    )
                rows['D'].append(
                    rozpis_txt
                )
                if colected_data.prijem:
                    rows['E'].append(
                        colected_data.partials[rozpis_key]
                    )
                else:
                    rows['E'].append(
                        None
                    )
                if colected_data.vydej:
                    rows['F'].append(
                        colected_data.partials[rozpis_key]
                    )
                else:
                    rows['F'].append(
                        None
                    )
                rows['H'].append(
                    rozpis_key
                )
        else:
            # copy row
            for c in tuple('BCDEFH'):
                rows[c].append(loaded_excel[sheetname]['{}{}'.format(c, r.row)].value)

    assert all([True if len(list(rows.items())[0][1]) == j else False for j in [len(i[1]) for i in rows.items()]])

    # now write to new excel book
    ef = openpyxl.load_workbook(new_path)
    rl = len(rows['B'])

    for i in range(rl):
        for c in rows.keys():
            ef[sheetname]['{}{}'.format(c, i + CONFIG.offset)].value = rows[c][i]

    ef.save(new_path)
    ef.close()
    return new_path


def mend_excel(source_path, new_path, sheetname='TPK1'):
    a = openpyxl.load_workbook(source_path, data_only=True)
    # data in U, V and D
    # flags are in U
    rows = {
        'B': [],
        'C': [],
        'D': [],
        'E': [],
        'F': [],
        'H': [],
    }
    for i, r in enumerate(get_cover_row_or_block(a[sheetname], 'U')):
        if i in range(CONFIG.offset - 1):
            continue
        if isinstance(r, list):
            # write data from cover
            colected_data = extract_data(a[sheetname], r)
            for j, (rozpis_key, rozpis_txt) in enumerate(colected_data.to_rows()):
                rows['B'].append(
                    colected_data.day
                )
                if j is 0:
                    rows['C'].append(
                        colected_data.doklad
                    )
                else:
                    rows['C'].append(
                        None
                    )
                rows['D'].append(
                    rozpis_txt
                )
                if colected_data.prijem:
                    rows['E'].append(
                        colected_data.partials[rozpis_key]
                    )
                else:
                    rows['E'].append(
                        None
                    )
                if colected_data.vydej:
                    rows['F'].append(
                        colected_data.partials[rozpis_key]
                    )
                else:
                    rows['F'].append(
                        None
                    )
                rows['H'].append(
                    rozpis_key
                )
        else:
            # copy row
            for c in tuple('BCDEFH'):
                rows[c].append(a[sheetname]['{}{}'.format(c, r.row)].value)

    assert all([True if len(list(rows.items())[0][1]) == j else False for j in [len(i[1]) for i in rows.items()]])

    # now write to new excel book
    ef = openpyxl.load_workbook(new_path)
    rl = len(rows['B'])
    # sheet = ef[sheetname]
    for i in range(rl):
        for c in rows.keys():
            ef[sheetname]['{}{}'.format(c, i + CONFIG.offset)].value = rows[c][i]
            # sheet['{}{}'.format(c, i + offset)].value = rows[c][i]
            # sheet.cell(coordinate='{}{}'.format(c, i + offset)).value = rows[c][i]
    ef.save(new_path)
    ef.close()
    print('written all')


def get_cover_row_or_block(sheet, column):
    k = iter(sheet[column])
    ucell = next(k)
    while ucell:
        if isinstance(sheet['B{}'.format(ucell.row)].value, str):
            if sheet['B{}'.format(ucell.row)].value.strip() == 'CELKEM':
                break
        if ucell.value is not None and ucell.value is not CONFIG.contsign:
            block = [ucell]
            ucell = next(k)
            while ucell.value == CONFIG.contsign:
                block.append(ucell)
                ucell = next(k)
            yield block
        else:
            yield ucell
            ucell = next(k)


if __name__ == '__main__':
    # testing
    import os
    path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            '..', 'example', 'pokl_example.xlsx',
        )
    )
    p0 = os.path.join('data', 'poklad_base.xlsx')
    p1 = 'p1_final.xlsx'
    copy(p0, p1)

    mend_excel(path, p1)
