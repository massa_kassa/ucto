import openpyxl
from operator import xor
from krydo.report import Bill, ReportError
from krydo.date_parse import dateparse
from krydo.config import CONFIG
import datetime


def get_cover_row(column):
    k = iter(column)
    ucell = next(k)
    while ucell:
        try:
            if ucell.value is not None and ucell.value is not CONFIG.contsign:
                block = [ucell]
                ucell = next(k)
                while ucell.value == CONFIG.contsign:
                    block.append(ucell)
                    ucell = next(k)
                yield block
            else:
                ucell = next(k)
        except StopIteration:
            break


def extract_data(sheet, cells):
    co = Cover()
    day = []
    ucel = []
    prijem = []
    vydej = []
    doklad_name = []
    name = []
    address = []
    kind = []
    for c in cells:
        day.append(sheet['{}{}'.format(CONFIG.day, c.row)])
        ucel.append(sheet['{}{}'.format(CONFIG.ucel, c.row)])
        prijem.append(sheet['{}{}'.format(CONFIG.prijem, c.row)])
        vydej.append(sheet['{}{}'.format(CONFIG.vydej, c.row)])
        # partials = sheet[CONFIG.rozpis.format(c.row, c.row)]
        # rozpis.append(partials)
        name.append(sheet['{}{}'.format(CONFIG.name, c.row)])
        address.append(sheet['{}{}'.format(CONFIG.address, c.row)])
        kind.append(sheet['{}{}'.format(CONFIG.kind, c.row)])
        doklad_name.append(c)

    # process doklad name
    # ===============================================================
    dname = [i for i in clean_list(doklad_name) if i is not CONFIG.contsign]

    if len(dname) is not 1:
        if len(dname) > 1:
            raise ReportError('Multiple dates in selected range')
        else:
            raise ReportError('No date in selected range ({}{})'.format(CONFIG.day, cells[0].row))
    co.doklad = dname[0]

    # process day
    # ===============================================================
    cleaned_day = []
    for d in clean_list(day):
        if isinstance(d, datetime.datetime):
            cleaned_day.append(d.strftime('%d.%m.%y'))
        elif d.strip():
            cleaned_day.append(d)

    # cleaned_day = [d for d in clean_list(day) if d.strip()]
    final_day = list(set(cleaned_day))

    if len(final_day) != 1:
        if len(final_day) > 1:
            raise ReportError('Multiple dates in selected range. (Days {})'.format(' '.join(final_day)))
        else:
            raise ReportError('No date in selected range ({}{})'.format(CONFIG.day, cells[0].row))

    # if not dateparse(final_day[0]):
    #     raise ReportError('Date not in correct format: for example use DD.MM.YYYY')
    co.day = final_day[0]

    # process ucel
    # ==============================================================
    lu = len(ucel)
    ucel_clean = clean_list(ucel)
    if lu != len(ucel_clean):
        raise ReportError('No description provided for some cell/s in selected range')
    co.ucel = ucel_clean

    # process prijem vydej
    # ==============================================================
    pr = clean_list(prijem)
    vy = clean_list(vydej)

    if any(vy) and not any(pr):
        co.vydej = sum(vy)
    elif any(pr) and not any(vy):
        co.prijem = sum(pr)
    else:
        raise ReportError('Cannot process mixed positive and negative transactions together')

    # process name
    # =============================================================
    name_clean = clean_list(name)
    if len(name_clean) != 1:
        if len(name_clean) > 1:
            raise ReportError('Multiple names provided for selected range')
        else:
            print('No name provided for selected range - assuming blank name.')
            co.name = ''
    else:
        co.name = name_clean[0]

    # process address
    # =============================================================
    address_clean = clean_list(address)
    if len(address_clean) != 1:
        if len(address_clean) > 1:
            raise ReportError('Multiple addresses provided for selected range')
        else:
            print('No address provided for selected range - assuming blank address.')
            co.address = ''
    else:
        co.address = address_clean[0]

    # process kind
    # ============================================================
    if not all(kind):
        raise ReportError('Payment type must be defined for all rows')

    wrap_partials = {i: 0 for i in {j.value for j in kind}}
    wrap_ucel = {i: [] for i in {j.value for j in kind}}
    for t, p, v, u in zip(kind, prijem, vydej, ucel):
        if p.value:
            wrap_partials[t.value] += p.value
        elif v.value:
            wrap_partials[t.value] += v.value
        else:
            raise ReportError
        wrap_ucel[t.value] += [u.value]

    for key in wrap_ucel.keys():
        wrap_ucel[key] = ' '.join(wrap_ucel[key])
    co.partials = wrap_partials
    co.partials_des = wrap_ucel

    return co


def clean_list(ii):
    return [i.value for i in ii if i.value]


class Cover(object):
    def __init__(self):
        self.day = None
        self.ucel = []
        self.prijem = None
        self.vydej = None
        self.partials = dict()
        self.partials_des = dict()
        self.name = ''
        self.address = ''
        self.doklad = ''

    def produce_pdf_cover(self, target):
        assert xor(self.prijem is not None, self.vydej is not None)

        if isinstance(target, str):
            pdf = open(target, 'wb')
            bill = Bill(pdf)
        else:
            bill = Bill(target)
        if self.prijem:
            bill.draw_prijal(
                doklad_num=self.doklad,
                doklad_date=self.day,
                person_name=self.name,
                person_address=self.address,
                ucel_l=self.ucel,
                total=self.prijem
            )
        elif self.vydej:
            bill.draw_vydal(
                doklad_num=self.doklad,
                doklad_date=self.day,
                person_name=self.name,
                person_address=self.address,
                ucel_l=self.ucel,
                total=self.vydej
            )
        else:
            raise ReportError('No inputs nor outputs in the data')

        bill.c.showPage()
        return bill.c

    def to_rows(self):
        """
        split collected rows by kind
        :return: generator
        """
        for key in self.partials.keys():
            yield key, self.partials_des[key]


def process_loaded_excel_ucto(loaded_exel, file):

    outfile = '.'.join(file.split('.')[:-1]) + '-doklady.pdf'
    page = None
    for i, r in enumerate(get_cover_row(loaded_exel['TPK1'][CONFIG.export_control])):
        one_frame = extract_data(loaded_exel['TPK1'], r)
        # ofs.append(one_frame)
        if page:
            page = one_frame.produce_pdf_cover(page)
        else:
            page = one_frame.produce_pdf_cover(outfile)
        print('kryci doklad {}'.format(i))
    if page:
        page.save()
    return outfile


def wrap_ucto(source_path):
    a = openpyxl.load_workbook(source_path, data_only=True)
    # a.get_sheet_by_name('TPK1')
    # data in U, V and D
    # flags are in U
    # ofs = []
    process_loaded_excel_ucto(a, source_path)
    return


if __name__ == '__main__':
    # testing
    path = ''
    wrap_ucto(path)
