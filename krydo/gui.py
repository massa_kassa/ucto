import os
import shutil
import wx
import openpyxl
from openpyxl.utils import exceptions as openxl_excp
from krydo.process_ucto import process_loaded_excel_ucto
from krydo.write_workbook import process_loaded_excel_new_workbook
from krydo.report import ReportError
from krydo.config import CONFIG


class UctoGui(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(UctoGui, self).__init__(*args, **kwargs)

        # data section
        self.file_path = None
        # stored value in self.doklady.Value
        self.doklady = None
        self.kniha = None

        self.tool_name = wx.StaticText(
            self,
            size=(80, 30),
            pos=(20, 20),
            label='Krydo',
        )
        self.tool_name.SetFont(
            wx.Font(
                18, wx.DECORATIVE, wx.ITALIC, wx.NORMAL, wx.BOLD
            )
        )
        # select file
        self.x_file_path = wx.TextCtrl(
            self,
            value="File/path/krydo.xlsx",
            pos=(20, 60),
            size=(200, -1),
        )
        self.x_file_path.SetToolTip(
            'Provide absolute path to excel file with data (you can use file selector)'
        )
        self.x_selectfile = wx.Button(
            self,
            label="Select file",
            pos=(225, 60),
            size=(80, -1)
        )
        self.Bind(wx.EVT_BUTTON, self.OnOpen, self.x_selectfile)

        self.doklady = wx.CheckBox(
            self,
            label="Exportovat kryci doklady.",
            pos=(20, 100)
        )
        self.doklady.SetToolTip(
            'Exportuje kryci doklady do pdf na zaklade zadanych udaju v excelovskem souboru.'
        )
        self.kniha = wx.CheckBox(
            self,
            label="Exportovat pokladni knihu.",
            pos=(20, 120)
        )
        self.kniha.SetToolTip(
            'Exportuje pokladni knihu s krycimy doklady. (Ponecha neoznacene polozky.)'
        )
        self.x_stredisko_name = wx.TextCtrl(
            self,
            value=CONFIG.billing['fakturacni_udaje'],
            pos=(52, 180),
            size=(221, 60),
            style=wx.TE_MULTILINE
        )
        self.stredisko_name = wx.StaticText(
            self,
            pos=(20, 160),
            label='Fakturacni udaje:'
        )
        self.btn_execute = wx.Button(
            self,
            label="Provest vybranne",
            pos=(175, 250),
            size=(130, 40)
        )
        self.Bind(wx.EVT_BUTTON, self.execute, self.btn_execute)

        self.loaded_excel_file = None

    def execute(self, evt):
        # setting "fakturacni_udaje" from textbox
        CONFIG.billing['fakturacni_udaje'] = self.x_stredisko_name.Value.replace('&nbsp;', chr(160))

        # read file and status and execute poklad_base and process krydo
        if not self.doklady.Value and not self.kniha.Value:
            wx.MessageBox('Nothing checked. Nothing to do.', 'Info', wx.OK | wx.ICON_INFORMATION)

        if self.file_path:
            try:
                self.loaded_excel_file = openpyxl.load_workbook(self.file_path, data_only=True)
            except (FileExistsError, FileNotFoundError) as e:
                wx.MessageBox('File not found: {}'.format(e))
            except openxl_excp.InvalidFileException as e:
                wx.MessageBox('File loading failed.: {}'.format(e), 'Error', wx.OK | wx.ICON_ERROR)
                return
        else:
            wx.MessageBox('No file selected. Nothing to do.', 'Info', wx.OK | wx.ICON_INFORMATION)
            return

        assert self.loaded_excel_file is not None
        done_msg = []
        # loaded_excel_file = None
        if self.doklady.Value:
            try:
                of = process_loaded_excel_ucto(self.loaded_excel_file, self.file_path)
                done_msg.append('zpracovany kryci doklady do: {}'.format(of))
            except ReportError as e:
                wx.MessageBox(e, 'Error', wx.OK | wx.ICON_ERROR)

        if self.kniha.Value:
            try:
                newexcel_path = '.'.join(self.file_path.split('.')[:-1]) + '-kryci_doklady.xlsx'
                shutil.copy(
                    os.path.abspath(
                        os.path.join(
                            os.path.dirname(__file__), 'data', 'poklad_base.xlsx'
                        )
                    ),
                    newexcel_path
                )
                of = process_loaded_excel_new_workbook(
                    loaded_excel=self.loaded_excel_file,
                    new_path=newexcel_path,
                )
                done_msg.append('zpracovana pokladni kniha do: {}'.format(of))
            except ReportError as e:
                wx.MessageBox(e, 'Error', wx.OK | wx.ICON_ERROR)

        wx.MessageBox('\n'.join(done_msg), 'Info', wx.OK | wx.ICON_INFORMATION)

    # @tryit_decorator
    def OnOpen(self, evt):
        # otherwise ask the user what new file to open
        with wx.FileDialog(self, "Open excel file", wildcard="excel files (*.xls/x)|*.xls?",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind

            # Proceed loading the file chosen by the user
            pathname = fileDialog.GetPath()
            self.file_path = pathname
            self.x_file_path.Value = pathname


def gui_main():
    # entry point for the app
    app = wx.App()
    frame = UctoGui(None, wx.ID_ANY, 'krydo')
    wx.Window.SetMinSize(frame, wx.Size(400, 300))
    frame.Show(True)
    app.MainLoop()


if __name__ == '__main__':
    gui_main()
