from setuptools import setup, find_packages
import os
import sys
if sys.version_info < (3, 4):
    sys.exit('Sorry, Python < 3.4 is not supported')

root_dir = os.path.join(os.path.dirname(__file__), 'krydo', 'data')
files2inlcude = []
for a, _, b in os.walk(root_dir):
    k = [os.path.join(root_dir, a, i) for i in b]
    files2inlcude += k

setup(
    name='Krydo',
    version='0.1.0',
    description='tool for generating bills for scout accounting',
    author='Massa Kassa',
    author_email='massa.kassa.sc3na@gmail.com',
    entry_points={
        'console_scripts': [
            'krydo = krydo.gui:gui_main'
        ],
    },
    install_requires=[
        'setuptools',
        'wxpython',
        'reportlab',
        'openpyxl',
        'num2words>=0.5.7',
    ],
    package_data={'krydo': files2inlcude},
    include_package_data=True,
    packages=find_packages()
)
