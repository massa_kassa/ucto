rem Check if the python is correct version
for /f "tokens=1-3 delims=. " %%i in ('python -V') do set PY_version=%%i %%j.%%k
if "%PY_version:~0,6%" == "Python" (
    ECHO We have Python
) ELSE (
    echo "Python not found - Please install python3.4 or newer.."
    pause
)
if "%PY_version:~7,-2%" EQU "3" (
    ECHO We have correct Python major
) ELSE (
    ECHO Python was not found in correct version. Install python3.4 or newer.
    pause
)
if "%PY_version:~-1%" GEQ "4" (
    ECHO We have correct Python minor
) ELSE (
    ECHO Python was not found in correct version. Install python3.4 or newer.
    pause
)

rem Check if we have pip3
for /f "tokens=1-3 delims=. " %%i in ('pip3 -V') do set PIP3_version=%%i %%j.%%k
if "%PIP3_version:~0,3%" EQU "pip" (
    echo We have pip3
) ELSE (
    ECHO pip3 was not found in PATH.
    pause
)

rem run update pip and install requirements
python -m pip install --upgrade pip
IF /I "%ERRORLEVEL%" NEQ "0" (
    ECHO pip3 update failed
    pause
)
pip3 install -r requirements.txt
IF /I "%ERRORLEVEL%" NEQ "0" (
    ECHO pip3 install required packages failed
    pause
)
echo If no error reported Installation succesfull.
pause