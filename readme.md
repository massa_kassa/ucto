# Krydo
App for processing very simple accounting information in specific format.

Krydo je nástroj pro skautské účetnictví, který z vyplněné tabulky s doklady
 vyrobí krycí doklady (.pdf) a účetní knihu s těmito doklady.

## Instalace
### Windows
- Install Python 3.4 and newer. Don't forget to click on add to PATH checkbox during installation.
- Download and unzip this repo (Downloads -> Download repository)
- Run install_win.bat batch script. (This will install all dependencies needed to run the app)

### Linux / windows commandline
- get Python 3.4.
- download and unzip this repo, luchn terminal there.
- Run:
```
# to update pip
pip3 install --update pip
```
#### Partial installation
Only dependencies will be installed and lunch will only be possible from within the directory.
```
pip3 install -r requirements.txt
# run
python3 -m ucto.gui
```
#### Full installation
Dependencies and this package will be installed. Lunch from anywhere.
```
# --- full installation ---
# to install
python3 setup.py install
# run
krydo
```

## Příprava pokladní knihy
Je nutné specifikovat které účtenky (řádky v pokladní knize) paří k sobě. Musí se označit.
 Označení provádíme do sloupce "U" (hned vpravo od sloupce KONTROLA ve starnartní pokladní knize).
 První účtenku z těch které mají být na společném dokladu označíme pořadovým číslem dokladu (V1, P2, atp),
 každou další účtenku označíme tečkou ".". Pokud k některým účtenkám nechceme vystavit krycí doklad nepíšeme nic.

 Dále je možno připsat jméno (sloupec "V") a adresu ("W"), které se přidají do krycího dokladu a nemusíte je pak vypisovat.
 
 **Prohlédněte si soubory ve složce "example"**
 
 [Vzor vstupního souboru.](example/pokl_example.xlsx)
 
 A jak to bude vypadat: [pdf](example/pokl_example-doklady.pdf), [excel](example/pokl_example-kryci_doklady.xlsx)

## Vygenerování krycích dokladů
1) Spustím program
2) Vybereme soubor (select file)
3) zatrhneme Doklady, Pokladní knihu, nebo oboje
4) Klikneme na "Provést vybranné"

Výstup je uložen do stejné složky z které jste vybrali vstupní pokladní knihu.
 V případě krycích dokladů je připojeno "-doklady.pdf".
 Pro pokladni knihu "-kryci_doklady.xlsx".

## Chyby - dej si pozor na
* Soubor musí být xls, nebo xlsx. Žádné další formáty nejsou podporovány (pro open/libre office ulozit jako xlsx)
* Pro řádky bez tečky nebo čísla dokladu nejsou vystaveny krycí doklady, ale jsou zahrnuty v exportované pokladní knize
 (např převod z/do )